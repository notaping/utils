package de.notepass.notaping.core.shared;

import java.util.Locale;
import java.util.ResourceBundle;

public class ResourceBundleUtils {
    public static String getLabel(ResourceBundle bundle, String id) {
        return getLabel(bundle, id, true, "??"+id+"??");
    }

    public static String getLabel(ResourceBundle bundle, String id, boolean localized) {
        return getLabel(bundle, id, localized, "??"+id+"??");
    }

    public static String getLabel(ResourceBundle bundle, String id, String fallback) {
        return getLabel(bundle, id, true, fallback);
    }

    public static String getLabel(ResourceBundle bundle, String id, boolean localized, String fallback) {
        if (!localized) {
            bundle = ResourceBundle.getBundle(bundle.getBaseBundleName(), Locale.ENGLISH);
        }

        if (bundle.containsKey(id)) {
            return bundle.getString(id);
        } else {
            return fallback;
        }
    }
}
