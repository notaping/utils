package de.notepass.notaping.core.shared;

import java.io.IOException;
import java.io.OutputStream;

/**
 * Special version of the output stream that returns the amount of bytes
 * written and doesn't throw an exception if the target disk is full
 */
public abstract class WrittenBytesAwareOutputStream extends OutputStream {
    public abstract int writeA(byte[] b) throws IOException;

    @Override
    public void write(byte[] b) throws IOException {
        writeA(b);
    }

    public abstract int writeA(byte[] b, int off, int len) throws IOException;

    @Override
    public void write(byte[] b, int off, int len) throws IOException {
        writeA(b, off, len);
    }

    public abstract int writeA(int b) throws IOException;

    @Override
    public void write(int b) throws IOException {
        writeA(b);
    }

    public abstract int flushA() throws IOException;

    @Override
    public void flush() throws IOException {
        flushA();
    }

    public abstract int getExpectedNextFlushSize();
}
