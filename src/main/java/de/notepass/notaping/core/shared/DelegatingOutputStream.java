package de.notepass.notaping.core.shared;

import java.io.OutputStream;

public interface DelegatingOutputStream<T extends OutputStream> {
    public T getDelegate();
}
