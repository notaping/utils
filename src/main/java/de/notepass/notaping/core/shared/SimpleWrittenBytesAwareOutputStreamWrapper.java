package de.notepass.notaping.core.shared;

import java.io.IOException;
import java.io.OutputStream;

/**
 * Wraps a given stream and always returns the amount of bytes to write as bytes written
 */
public class SimpleWrittenBytesAwareOutputStreamWrapper extends WrittenBytesAwareOutputStream {
    private final OutputStream parent;

    /**
     * Wraps a given stream and always returns the amount of bytes to write as bytes written
     */
    public SimpleWrittenBytesAwareOutputStreamWrapper(OutputStream parent) {
        this.parent = parent;
    }

    @Override
    public int writeA(byte[] b) throws IOException {
        parent.write(b);
        return b.length;
    }

    @Override
    public int writeA(byte[] b, int off, int len) throws IOException {
        parent.write(b, off, len);
        return len;
    }

    @Override
    public int writeA(int b) throws IOException {
        parent.write(b);
        return 1;
    }

    @Override
    public int flushA() throws IOException {
        parent.flush();
        return 0;
    }

    @Override
    public int getExpectedNextFlushSize() {
        return 0;
    }
}
