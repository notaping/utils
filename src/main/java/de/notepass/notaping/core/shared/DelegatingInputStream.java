package de.notepass.notaping.core.shared;

import java.io.InputStream;

public interface DelegatingInputStream<T extends InputStream> {
    public T getDelegate();
}
