package de.notepass.notaping.core.shared;

import java.io.IOException;

public class NoopOutputStream extends WrittenBytesAwareOutputStream {
    @Override
    public int writeA(byte[] b) throws IOException {
        return b.length;
    }

    @Override
    public int writeA(byte[] b, int off, int len) throws IOException {
        return len;
    }

    @Override
    public int writeA(int b) throws IOException {
        return 1;
    }

    @Override
    public int flushA() throws IOException {
        return 0;
    }

    @Override
    public int getExpectedNextFlushSize() {
        return 0;
    }
}
