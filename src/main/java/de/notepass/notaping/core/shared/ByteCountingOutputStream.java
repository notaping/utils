package de.notepass.notaping.core.shared;

import java.io.IOException;
import java.io.OutputStream;

public class ByteCountingOutputStream extends WrittenBytesAwareOutputStream implements DelegatingOutputStream {
    private long bytesWritten = 0;
    private WrittenBytesAwareOutputStream parent = null;

    public ByteCountingOutputStream(WrittenBytesAwareOutputStream parent) {
        this.parent = parent;
    }

    public ByteCountingOutputStream() {
        parent = new NoopOutputStream();
    }

    @Override
    public int writeA(byte[] b) throws IOException {
        int written = parent.writeA(b);
        bytesWritten += written;
        return written;
    }

    @Override
    public int writeA(byte[] b, int off, int len) throws IOException {
        int written = parent.writeA(b, off, len);
        bytesWritten += written;
        return written;
    }

    @Override
    public int writeA(int b) throws IOException {
        int written = parent.writeA(b);
        bytesWritten += written;
        return written;
    }

    @Override
    public int flushA() throws IOException {
        int written = parent.flushA();
        bytesWritten += written;
        return written;
    }

    @Override
    public int getExpectedNextFlushSize() {
        return parent.getExpectedNextFlushSize();
    }

    public long getBytesWritten() {
        return bytesWritten;
    }

    @Override
    public OutputStream getDelegate() {
        return parent;
    }
}
